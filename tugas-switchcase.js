// let menu = prompt('Masukkan menu');
// switch(menu) {
//     case 'sayur':
//         alert('healthy food')
//         break;
//     case 'buah':
//         alert('healthy food')
//         break;
//     case 'susu':
//         alert('healthy drink')
//         break;
//     case 'nasi':
//         alert('unhealthy food')
//         break;
//     case 'softdrink':
//         alert('unhealthy food')
//         break;
//     case 'burger':
//         alert('unhealthy food')
//         break;
//     case 'pizza':
//         alert('unhealthy food')
//         break;
//     default:
//         alert('not in my database')
// }

let menu = prompt('Write your menu')

switch (menu) {
    case 'sayur':
    case 'buah':
        alert('healthy food')
        break;
    case 'pizza':
    case 'instant noodle':
        alert('unhealthy food')
        break;
    default:
        alert('not known')
}

let animal = prompt('Write your favorite animal')

switch (animal) {
    case 'cat':
    case 'dog':
        alert('tame')
        break;
    case 'lion':
    case 'tiger':
        alert('wild')
        break;
    default:
        alert('unknown')
}

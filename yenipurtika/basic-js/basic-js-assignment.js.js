let arrPenumpang = ["mia", null, "jawara", "yeye", null, "budi"]

function tambahPenumpang(nama) {
    let penumpangKosong = arrPenumpang.indexOf(null);
    if (arrPenumpang.includes(nama)) { 
        console.log(`Maaf, ${nama} sudah ada di dalam bus`);
    }else if (arrPenumpang.includes(null)) {
        let penumpangBaru = arrPenumpang.splice(penumpangKosong, 1, nama);
    }else if (arrPenumpang.length == 8) {
        console.log(`Maaf, bus sudah penuh`);
    }else {
        console.log(arrPenumpang.push(nama));
    }
    return arrPenumpang;
}

function turunPenumpang (nama) {
    let penumpangTurun = arrPenumpang.indexOf(nama);
    let busKosong = (isiBus => isiBus === null);
    if (arrPenumpang.includes(nama)) {
        let penumpangBaruTurun = arrPenumpang.splice(penumpangTurun, 1, null);
    }else if (nama == 'all') {
        let semuaPenumpang = arrPenumpang.splice(0, 8, null);    
        console.log(`Semua penumpang sudah turun`);
    }else if (arrPenumpang.every(busKosong)) {
        console.log(`Maaf, bus kosong`);
    }else {
        console.log(`Maaf, ${nama} tidak berada di dalam bus`);
    }
    return arrPenumpang;
}

//Framework
// Tambah Penumpang
//1. temukan index dalam array yang mana yg null atau undefined
//2. jika ada maka gantikan index tersebut dgn penumpang lain
//3. jika tidak ada maka penumpang baru ditambahkan ke belakang array
//4. jika kapasitas penumpang (8 orang) sudah terisi, maka muncul notif bus sudah penuh
//5. jika ada kursi kosong, tambahkan dulu di index yang paling kecil

//Turun Penumpang
//1. temukan nama dalam index array, jika ada hapus index tersebut ganti dengan null
//2. jika nama yg diinput tidak ada dalam index, maka muncul notif nama tersebut tidak ada dalam bus
//3. jika ingin penumpang turun semua, maka temukan index pertama dalam array, lalu hapus semua penumpang ganti dengan null, dan ada notif semua penumpang sudah turun. kemudian nanti bisa ditambahkan penumpang lagi.
//4. jika dalam array tidak ada penumpang atau null, lalu diinput nama untuk turun, maka muncul notif maaf bus sudah kosong.
